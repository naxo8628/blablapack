CREATE DATABASE  IF NOT EXISTS `blablapack` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `blablapack`;
-- MySQL dump 10.13  Distrib 5.6.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: blablapack
-- ------------------------------------------------------
-- Server version	5.6.24-0ubuntu2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Auction`
--

DROP TABLE IF EXISTS `Auction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Auction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `origin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `expired` datetime NOT NULL,
  `periodicity` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `travel_id` int(11) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `num_bids` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1159CC0F19EB6921` (`client_id`),
  KEY `IDX_1159CC0F12469DE2` (`category_id`),
  KEY `IDX_1159CC0FECAB15B3` (`travel_id`),
  CONSTRAINT `FK_1159CC0F12469DE2` FOREIGN KEY (`category_id`) REFERENCES `Category` (`id`),
  CONSTRAINT `FK_1159CC0F19EB6921` FOREIGN KEY (`client_id`) REFERENCES `User` (`id`),
  CONSTRAINT `FK_1159CC0FECAB15B3` FOREIGN KEY (`travel_id`) REFERENCES `Travel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Auction`
--

LOCK TABLES `Auction` WRITE;
/*!40000 ALTER TABLE `Auction` DISABLE KEYS */;
INSERT INTO `Auction` VALUES (1,58,NULL,'Sevilla','Madrid',1,'2015-05-24 07:43:14','2015-05-25 07:43:14',0,0,NULL,0,0),(2,60,NULL,'Sevilla','Madrid',1,'2015-05-24 07:43:40','2015-05-25 07:43:40',0,0,NULL,0,0),(3,61,NULL,'Sevilla','Madrid',12,'2015-05-24 07:45:26','2015-06-05 07:45:26',0,0,NULL,0,0),(4,63,NULL,'Sevilla','Madrid',2,'2015-05-24 07:46:11','2015-05-26 07:46:11',0,0,NULL,0,0),(5,64,NULL,'Sevilla','Madrid',1,'2015-05-24 07:46:58','2015-05-25 07:46:58',0,0,NULL,0,0),(6,65,NULL,'Sevilla','Madrid',2,'2015-05-24 07:48:21','2015-05-26 07:48:21',0,0,NULL,0,0),(7,67,NULL,'Sevilla','Madrid',2,'2015-05-24 08:02:38','2015-05-26 08:02:38',0,0,NULL,0,0),(8,68,NULL,'Sevilla','Madrid',2,'2015-05-24 08:03:12','2015-05-26 08:03:12',0,0,NULL,0,0),(9,69,NULL,'Sevilla','Madrid',2,'2015-05-24 08:04:23','2015-05-26 08:04:23',0,0,NULL,0,0),(10,70,NULL,'Sevilla','Madrid',12,'2015-05-24 08:05:10','2015-06-05 08:05:10',0,0,NULL,0,0),(11,NULL,NULL,'Madrid','Sevilla',3,'2015-05-24 11:02:24','2015-05-27 11:02:24',0,0,NULL,0,0),(12,NULL,NULL,'Honduras','antequera',3,'2015-05-24 11:03:35','2015-05-27 11:03:35',0,0,NULL,0,0),(13,71,NULL,'sevilla','Madrid',3,'2015-05-24 13:30:51','2015-05-27 13:30:51',0,0,NULL,0,0),(14,72,NULL,'Sevilla','Madrid',6,'2015-05-24 13:35:43','2015-05-30 13:35:43',0,0,NULL,0,0),(15,NULL,NULL,'Madrid','Sevilla',3,'2015-05-24 13:37:41','2015-05-27 13:37:41',0,0,NULL,0,0),(16,23,NULL,'Sevilla','Madrid',3,'2015-06-01 20:02:37','2015-06-04 20:02:37',0,0,NULL,0,0),(17,74,NULL,'Calle Pablo Sorozábal, 1, 41928 Palomares del Río, Sevilla, Spain','Calle Arturo Soria, 120, 28027 Madrid, Madrid, Spain',72,'2015-06-01 23:57:34','2015-06-04 23:57:34',0,0,NULL,0,0),(18,74,NULL,'Sevilla','Madrid',72,'2015-06-02 00:04:03','2015-06-05 00:04:03',0,0,NULL,0,0),(19,74,NULL,'Calle Pablo Sorozábal, 1, 41928 Palomares del Río, Sevilla, Spain','Calle Arturo Soria, 120, 28027 Madrid, Madrid, Spain',72,'2015-06-02 00:06:48','2015-06-05 00:06:48',0,0,NULL,0,0),(20,75,NULL,'Pontevedra, Pontevedra, Spain','Lugo, Lugo, Spain',26,'2015-06-02 00:41:41','2015-06-03 02:41:41',0,0,NULL,0,0),(21,NULL,NULL,'Madrid','Sevilla',12,'2015-06-02 01:15:05','2015-06-14 01:15:05',0,0,NULL,0,0),(22,NULL,NULL,'Sevilla','Madrid',4,'2015-06-02 01:17:21','2015-06-06 01:17:21',0,0,NULL,0,0),(23,NULL,NULL,'Sevilla','Madrid',4,'2015-06-02 01:23:14','2015-06-03 15:23:14',0,0,NULL,0,0),(24,23,NULL,'Madrid, Madrid, Spain','Basque Country, Spain',216,'2015-06-02 04:37:01','2015-06-11 04:37:01',0,0,NULL,0,0),(25,23,NULL,'Alabama, USA','Texas, USA',36,'2015-06-02 04:44:29','2015-06-03 16:44:29',0,0,NULL,0,0),(26,23,NULL,'Piazza S. Giovanni, 1, Ala TN, Italy','Milan, Italy',42,'2015-06-02 04:48:02','2015-06-03 22:48:02',0,0,NULL,0,0),(27,23,NULL,'Chiclana, Cádiz, Spain','11140 Conil, Cádiz, Spain',24,'2015-06-02 04:50:43','2015-06-03 04:50:43',0,0,NULL,NULL,0),(28,76,NULL,'Sevilla','Madrid',28,'2015-06-02 06:02:40','2015-06-03 10:02:40',0,0,NULL,NULL,0),(29,23,NULL,'Cádiz, Cádiz, Spain','Seville, Sevilla, Spain',48,'2015-06-03 00:18:01','2015-06-05 00:18:01',0,0,20,10,0),(30,23,NULL,'Cádiz, Cádiz, Spain','Seville, Sevilla, Spain',26,'2015-06-03 01:21:33','2015-06-04 03:21:33',0,0,18,10,0),(31,23,NULL,'Cádiz, Cádiz, Spain','Seville, Sevilla, Spain',31,'2015-06-03 01:48:45','2015-06-04 08:48:45',0,0,18,32,2),(32,23,NULL,'Madrid, Madrid, Spain','Basque Country, Spain',171,'2015-06-03 14:14:01','2015-06-10 17:14:01',0,0,21,20,1),(33,83,NULL,'Seville, Sevilla, Spain','Calle Pablo Sorozabal, 28330 San Martín de la Vega, Madrid, Spain',24,'2015-06-03 20:04:33','2015-06-04 20:04:33',0,0,23,49,2);
/*!40000 ALTER TABLE `Auction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Bid`
--

DROP TABLE IF EXISTS `Bid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `travel_id` int(11) DEFAULT NULL,
  `auction_id` int(11) DEFAULT NULL,
  `price` decimal(10,0) NOT NULL,
  `created` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_72BFF513ECAB15B3` (`travel_id`),
  KEY `IDX_72BFF51357B8F0DE` (`auction_id`),
  CONSTRAINT `FK_72BFF51357B8F0DE` FOREIGN KEY (`auction_id`) REFERENCES `Auction` (`id`),
  CONSTRAINT `FK_72BFF513ECAB15B3` FOREIGN KEY (`travel_id`) REFERENCES `Travel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Bid`
--

LOCK TABLES `Bid` WRITE;
/*!40000 ALTER TABLE `Bid` DISABLE KEYS */;
INSERT INTO `Bid` VALUES (1,16,23,12,'2015-06-02 06:50:36',0),(2,18,30,50,'2015-06-03 01:22:10',0),(4,18,31,40,'2015-06-03 02:08:23',0),(5,20,31,39,'2015-06-03 03:47:04',0),(6,18,31,38,'2015-06-03 04:32:42',0),(7,20,31,37,'2015-06-03 05:19:36',0),(8,20,29,10,'2015-06-03 05:30:20',0),(9,18,31,36,'2015-06-03 05:55:25',0),(10,20,31,35,'2015-06-03 07:25:47',0),(11,18,31,34,'2015-06-03 07:26:36',0),(12,18,30,10,'2015-06-03 13:39:04',0),(13,20,31,33,'2015-06-03 14:02:38',0),(14,18,31,32,'2015-06-03 14:04:33',0),(15,21,32,20,'2015-06-03 14:14:36',0),(16,22,33,50,'2015-06-03 20:08:19',0),(17,23,33,49,'2015-06-03 20:18:11',0);
/*!40000 ALTER TABLE `Bid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Category`
--

DROP TABLE IF EXISTS `Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Category`
--

LOCK TABLES `Category` WRITE;
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;
INSERT INTO `Category` VALUES (1,'carga normal'),(2,'alimentos'),(5,'animales'),(7,'asasssd');
/*!40000 ALTER TABLE `Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Certificate`
--

DROP TABLE IF EXISTS `Certificate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Certificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'hola',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Certificate`
--

LOCK TABLES `Certificate` WRITE;
/*!40000 ALTER TABLE `Certificate` DISABLE KEYS */;
INSERT INTO `Certificate` VALUES (1,'Test Post 1','prueba description','d2'),(2,'prueba','prueba description','as'),(3,'sd','sd3','note-add'),(4,'prueba','prueba description','note-add'),(5,'prueba','prueba description','note-add'),(6,'prueba','prueba description','note-add'),(7,'prueba','prueba description','note-add'),(8,'prueba','as','asas'),(9,'prueba','as','note-add'),(10,'prueba','as','note-add2'),(11,'prueba','a','note-add2'),(12,'prueba',' ','note-add2'),(13,'prueba','  ','note-add2');
/*!40000 ALTER TABLE `Certificate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Halt`
--

DROP TABLE IF EXISTS `Halt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Halt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `travel_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` decimal(10,0) NOT NULL,
  `latitude` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A8D19F57ECAB15B3` (`travel_id`),
  CONSTRAINT `FK_A8D19F57ECAB15B3` FOREIGN KEY (`travel_id`) REFERENCES `Travel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Halt`
--

LOCK TABLES `Halt` WRITE;
/*!40000 ALTER TABLE `Halt` DISABLE KEYS */;
/*!40000 ALTER TABLE `Halt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Package`
--

DROP TABLE IF EXISTS `Package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auction_id` int(11) DEFAULT NULL,
  `weight` decimal(10,0) NOT NULL,
  `height` decimal(10,0) NOT NULL,
  `width` decimal(10,0) NOT NULL,
  `length` decimal(10,0) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_11D55E0957B8F0DE` (`auction_id`),
  CONSTRAINT `FK_11D55E0957B8F0DE` FOREIGN KEY (`auction_id`) REFERENCES `Auction` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Package`
--

LOCK TABLES `Package` WRITE;
/*!40000 ALTER TABLE `Package` DISABLE KEYS */;
INSERT INTO `Package` VALUES (1,1,3,10,50,20,''),(2,2,3,10,50,20,''),(3,3,3,10,50,20,''),(4,4,3,10,50,20,''),(5,5,3,10,50,20,''),(6,6,3,10,50,20,''),(7,7,3,10,50,20,''),(8,8,3,10,50,20,''),(9,9,3,10,50,20,''),(10,10,3,10,50,20,''),(11,13,3,10,50,20,''),(12,14,3,10,50,20,''),(13,16,3,10,50,20,''),(14,17,12,30,20,40,'sofa'),(15,18,0,0,0,0,''),(16,19,12,20,30,40,'Sofa'),(17,20,12,20,30,40,'mueble bar'),(18,20,50,20,50,60,'cama doble'),(19,24,12,10,20,30,'perro mojado'),(20,25,2,1,2,2,'perro mojado'),(21,26,12,5,5,5,'cogorza'),(22,27,2,200,20,5,'tabla surf'),(23,27,5,50,100,100,'traje buceo'),(24,28,0,0,0,0,'sofa'),(25,29,56,20,30,50,'otro perro'),(26,30,45,12,25,655,'rincor de pensar'),(27,31,10,200,300,50,'paquete 1'),(28,31,12,20,50,200,'paquete 2'),(29,32,1,2,2,2,'vacio'),(30,33,12,20,30,40,'sofa');
/*!40000 ALTER TABLE `Package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Stop`
--

DROP TABLE IF EXISTS `Stop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Stop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` decimal(10,0) NOT NULL,
  `longitude` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Stop`
--

LOCK TABLES `Stop` WRITE;
/*!40000 ALTER TABLE `Stop` DISABLE KEYS */;
/*!40000 ALTER TABLE `Stop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Travel`
--

DROP TABLE IF EXISTS `Travel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Travel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipper_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `origin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2AA76EF838459F23` (`shipper_id`),
  KEY `IDX_2AA76EF812469DE2` (`category_id`),
  CONSTRAINT `FK_2AA76EF812469DE2` FOREIGN KEY (`category_id`) REFERENCES `Category` (`id`),
  CONSTRAINT `FK_2AA76EF838459F23` FOREIGN KEY (`shipper_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Travel`
--

LOCK TABLES `Travel` WRITE;
/*!40000 ALTER TABLE `Travel` DISABLE KEYS */;
INSERT INTO `Travel` VALUES (2,34,1,'Madrid','Sevilla',0,'2015-05-23 14:41:00','2015-05-22 14:41:01'),(3,34,1,'Madrid','Sevilla',0,'2015-05-24 15:07:30','2015-05-22 15:07:30'),(4,34,NULL,'milan','Sevilla',0,'2015-05-22 15:14:38','2015-05-22 15:14:38'),(5,34,NULL,'milan','Sevilla',0,'2015-05-22 18:27:20','2015-05-22 18:27:20'),(6,34,NULL,'milan','Sevilla',0,'2015-05-22 18:28:32','2015-05-22 18:28:32'),(7,34,NULL,'Madrid','Sevilla',0,'2015-05-24 00:00:00','2015-05-22 19:52:59'),(8,34,NULL,'Madrid','Sevilla',0,'2015-05-24 00:00:00','2015-05-22 20:01:17'),(9,34,NULL,'Madrid','Sevilla',0,'2015-05-27 00:00:00','2015-05-22 20:02:08'),(10,34,NULL,'milan','roma',0,'2015-05-30 00:00:00','2015-05-22 23:26:59'),(11,34,NULL,'Madrid','Sevilla',0,'2015-05-23 17:34:03','2015-05-23 17:34:28'),(12,34,NULL,'palomares del rio sevilla','parque porzuna sevilla',0,'2015-05-23 17:34:03','2015-05-23 17:35:12'),(13,34,NULL,'plasencia','rivera maya',0,'2015-05-23 21:39:48','2015-05-23 21:40:54'),(14,34,NULL,'alaska','retiro',0,'2015-05-28 00:00:00','2015-05-23 21:42:48'),(15,34,NULL,'Salamanca','Cordoba',0,'2015-07-02 00:00:00','2015-05-24 12:06:58'),(16,26,NULL,'Sevilla','Madrid',0,'2015-06-04 00:00:00','2015-06-02 05:06:12'),(17,26,NULL,'Sevilla','Madrid',0,'2015-06-04 00:00:00','2015-06-02 23:56:53'),(18,26,NULL,'cadiz','Sevilla',0,'2015-06-05 00:17:00','2015-06-03 00:17:00'),(19,81,NULL,'cadiz','Sevilla',0,'2015-06-05 03:20:00','2015-06-03 03:20:17'),(20,82,NULL,'Cadiz','Sevilla',0,'2015-06-05 03:29:00','2015-06-03 03:29:34'),(21,26,NULL,'Madrid','basque country',0,'2015-06-11 14:08:00','2015-06-03 14:08:05'),(22,84,NULL,'Sevilla','madrid',0,'2015-06-04 20:08:00','2015-06-03 20:08:04'),(23,85,NULL,'Sevilla','Madrid',0,'2015-06-04 20:10:00','2015-06-03 20:10:29');
/*!40000 ALTER TABLE `Travel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `last_conection` datetime NOT NULL,
  `validate_email` int(11) NOT NULL,
  `validate_phone` int(11) NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `configuration` int(11) DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transport_type` int(11) DEFAULT NULL,
  `transport_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_2DA17977D1B862B8` (`hash`),
  UNIQUE KEY `UNIQ_2DA17977E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (23,'Nacho','Visedo Salvat','naxo8628@gmail.com','$2y$12$PqJ8cMAGBzGAOBW14PoEj.vWbxn31jFx0mrrKAg3vni.LwWIDLLny',678109883,'2015-05-21 19:41:47','2015-05-21 19:41:47',0,0,'client',10101,NULL,NULL,NULL,'8587ba357336447e65af3ee3cd62346fede35612c1d805d53cca9245de0b2073'),(26,'Nacho','Visedo Salvat','shipper@gmail.com','$2y$12$CjFFU0//b.mx7sbIefazzeu5IwbRPrkhPFobsix1QeSewdTyPINzi',678109883,'2015-05-21 23:26:25','2015-05-21 23:26:25',0,0,'shipper',10101,'compañia',0,'buen coche','969466e8cd11cfe6e4a827a46b00a79e99f1d36f56f4d4fb6beac1bcad7bc868'),(27,'Nacho','Visedo Salvat','shipper2@gmail.com','$2y$12$uVzcYV./GicH712/Zz912eU9j4p.6Xc0KL8s/wVPZ0jvphEkFOO4q',678109883,'2015-05-21 23:30:50','2015-05-21 23:30:50',0,0,'shipper',10101,'compañia',0,'buen coche','1126f28bed1678ae40f8099aa670cb8cf94c47e5587b3e2de8d89f52d834dd80'),(28,'Nacho','Visedo Salvat','shipper3@gmail.com','$2y$12$i9AS2y4Td8sguEcAAgqY5.cSgDPeyKB5GIB8P35Itcj151zttV4au',678109883,'2015-05-21 23:31:06','2015-05-21 23:31:06',0,0,'shipper',10101,NULL,NULL,NULL,'1bd477a7620a77b47825a47b8c64c8f19824d0b28dd5391ba3e5c751b36ff155'),(29,'Nacho','Visedo Salvat','shipper5@gmail.com','$2y$12$7Vzim3K2Pnl6.T8SNLMx4eoRB2WIrrD.PuDjJFSONQgnUiYcaQ9Ie',678109883,'2015-05-21 23:42:56','2015-05-21 23:42:56',0,0,'shipper',10101,'  ',0,'  ','044f4ba7594d6c57a127448152fd506353adf6bd40b8a91ecac2acc3f7e20d97'),(31,'Nacho','Visedo Salvat','shipper6@gmail.com','$2y$12$K6QzcWDbQGreXS/fIwI/beBtR8omDBTHDbKAipp3BSEYdKGjhqL/2',678109883,'2015-05-21 23:43:31','2015-05-21 23:43:31',0,0,'shipper',10101,'  ',1,'  ','87d9964638dc93f5e712b7a2a10d78c7ef699329ff772288772fa94b617ed57a'),(33,'Nacho','Visedo Salvat','shipper7@gmail.com','$2y$12$tyCLbsUdBcPyDXh5h8MPT.CfmK9wvV6PwRQ3rhJBt.of2vIrfHpxC',678109883,'2015-05-21 23:45:10','2015-05-21 23:45:10',0,0,'shipper',10101,'  ',0,'  ','32ef0fbcc63d3a3fe2ae6951fe9c9338671e158696b5d16a8813eec2a83a365b'),(34,'Nacho','Visedo Salvat','shipper8@gmail.com','$2y$12$043phNwk5QU35HbmTyEitevP.sHj3jEYsIkEmPsr7A2O7vK0YgUz.',678109883,'2015-05-21 23:45:41','2015-05-21 23:45:41',0,0,'shipper',10101,'  ',0,'  ','3b5bc552454f6f458b995cbe748c574f0b59d362b8d40f9d0dd712508fdab866'),(36,'Nacho','Visedo Salvat','shipper9@gmail.com','$2y$12$3rZINhKQ5jyZVdRhAbyMAudUSGjpOIpn9JeeRePe./.82C0QVUxX.',678109883,'2015-05-21 23:45:55','2015-05-21 23:45:55',0,0,'shipper',10000,'  ',0,'  ','5de58c313fa988fdf8d9cafe04ef49dbac2848cfc35e34d219995b2688b87587'),(37,'Nacho','Visedo Salvat','naxo862823@gmail.com','$2y$12$/Ep125AgoWoMTCAqYnyzDerXczWccBqBxo8lHj9nDFExRb/4frvm.',678109883,'2015-05-22 00:28:08','2015-05-22 00:28:08',0,0,'client',10101,NULL,NULL,NULL,'efffba52d1bf3ecc386615a95fd66eb64646b89041feb6013cd3e534655a0469'),(38,'a','a','a@a.com','$2y$12$OpQ8gyILKDsQMKOwsdA3uO0oHxPNAa0p6eBTqbI7IlJe1WLPCopay',0,'2015-05-22 00:34:56','2015-05-22 00:34:56',0,0,'client',10000,NULL,NULL,NULL,'2189d27a7f94e779af093f1e827cdfa00be314194aa1fb94ecc71553bcd02a5e'),(39,'admin',' ','admin@gmail.com','$2y$12$OpQ8gyILKDsQMKOwsdA3uO0oHxPNAa0p6eBTqbI7IlJe1WLPCopay',0,'2015-05-22 00:34:56','2015-05-22 00:34:56',0,0,'admin',NULL,NULL,NULL,NULL,'2189d27a7f94e779af093f1e827cdfa00be314194aa1fb94ecc71553bcd02a5r'),(40,'shipper','as','shipperFront@gmail.com','$2y$12$atpdqaDYkW6X28CXTtd7rejVqikE.YGdqFIInzWrvber6vTfRIYUe',678109883,'2015-05-22 01:02:34','2015-05-22 01:02:34',0,0,'shipper',10000,'MRV',0,'  ','65ad6edcf73d27ebfcad22ea018103eed9fb6d434383759e6c3146378569e2fe'),(41,'Prueba','User','usuario@gmail.com','$2y$12$yBSaeaFOyHY8bu51a8vCZ.vMOP8Vzajj3A3x59S3n3I4XX5tJ7VQq',0,'2015-05-23 18:45:14','2015-05-23 18:45:14',0,0,'client',10000,NULL,NULL,NULL,'39a768e1fac8232edb28e7ded30bc8c4127910df26df1e57536b9db8185ad492'),(42,'a','a','a','$2y$12$ZbqgJ.P7voihXxtVsb/g4eOTsqDGiBZ..nWWtAj/rnMmufn6BHMbq',0,'2015-05-24 04:54:50','2015-05-24 04:54:50',0,0,'client',10000,NULL,NULL,NULL,'1bd00af9e128112f52405c6cffd595634b7d654afac5747d1be413c7e025a178'),(43,'a21','a2','a@aasas.com','$2y$12$LQSNgKuG3i2LejkIU0QJku4KmZAbvoaOyS7q5uTnQQYjWRblm2/Ka',0,'2015-05-24 04:57:46','2015-05-24 04:57:46',0,0,'client',10000,NULL,NULL,NULL,'6dfa3ad38a0bf82494ed77731423a63ac0120cc0030d1c65b7fefd3f5896964d'),(44,'a212','a2','2a@aasas.com','$2y$12$wnAPx2lFtFgWEj3R8tJcTeVdokIFfQLEs1U5x6QvE/xsFjT6Adl9O',0,'2015-05-24 04:58:14','2015-05-24 04:58:14',0,0,'client',10000,NULL,NULL,NULL,'f844028ca2892622cc2aa33f6df09dae543b1d33c83f576dd7156c85b098304e'),(46,'a212','a2','aas2a@aasas.com','$2y$12$L/s0jaaBhxLuFrz9Jo1In.evSahhOWnmDJSG9a1CQJwXqh878Z4kO',0,'2015-05-24 05:03:32','2015-05-24 05:03:32',0,0,'client',10000,NULL,NULL,NULL,'202b358505e70dd6b85f9aa24255fd7ef4234856b4815ac4e94f90318ee5de9d'),(47,'a212','a2','aasa2a@aasas.com','$2y$12$7/UAZNz7IrxLfIYotxJmIOW3ErhyYZjluZMiyjMLzYZv4C2oHy3iC',0,'2015-05-24 05:06:34','2015-05-24 05:06:34',0,0,'client',10000,NULL,NULL,NULL,'f2eb8aeb4a20f520d90e70ee79ad4aab6857b21e6906a51c38059e4b222aafcb'),(48,'a212','a2','aasa2asa@aasas.com','$2y$12$SkimHNpd6SIbnxtqqtZ6HOArLJPLO9Fy8OIj7hzBdteYUP2Pi.6y6',0,'2015-05-24 05:08:53','2015-05-24 05:08:53',0,0,'client',10000,NULL,NULL,NULL,'43be819b87220244cf7d30d5931bcd0b4f29aeed98b7e676362b3920b46b44a7'),(49,'a212','a2','aasa2asa3@aasas.com','$2y$12$QliNJYbMsn/v4yzuqEbyKuRPVESOiIVxUXjra.yEFN.38yv.OknBW',0,'2015-05-24 05:10:53','2015-05-24 05:10:53',0,0,'client',10000,NULL,NULL,NULL,'4668a4ef1ae611aec1b280f7bab71e88076ff7ba34398135f67a0022361a9873'),(50,'a212','a2','aasa2asa3a@aasas.com','$2y$12$WgarH0NG8b.bXnGvb6nq0e650loff6aWKGJU3tc3BFZZ1QhtRwvHW',0,'2015-05-24 05:12:40','2015-05-24 05:12:40',0,0,'client',10000,NULL,NULL,NULL,'8d78cde68ab18298e725178f3aa0f4dc15e6794f6b5f7fe18fa594829e0753dd'),(51,'a212','a2','aasa2aasa3a@aasas.com','$2y$12$MkCA5z7g4tNUfcNUBsNUiOsq/pHssQWeJ90chd8V9.tFQWmw3RHN.',0,'2015-05-24 05:13:05','2015-05-24 05:13:05',0,0,'client',10000,NULL,NULL,NULL,'255cd7597ffa58e58ea856c1b83405cc3d9211e2374514b5cfc3906775cfc185'),(52,'a212','a2','aasa2aasaaa3a@aasas.com','$2y$12$MQfUJ2XxyvlYFQAkNBsujeA7XHC3weXEvEI7SeD0OZbWzPDq2MGTO',0,'2015-05-24 05:13:36','2015-05-24 05:13:36',0,0,'client',10000,NULL,NULL,NULL,'5fe12b2c164c27aac7c83ca7f689acce55c0e4cf2c5d401a689324b1fe0deb4c'),(53,'a212','a2','aasa2aasaaaa3a@aasas.com','$2y$12$.9GmFKbM5LVEKi6j/HqMQuggHqG47hd1uqhd9EuNRxlQrGpPgfs4O',0,'2015-05-24 05:14:21','2015-05-24 05:14:21',0,0,'client',10000,NULL,NULL,NULL,'72c5ea5c5dd1e1b1ac3eda8b8bdf809883494ffad64710c86dd07d5f0c191ce7'),(58,'pruebaCliente','surname','clienteAuction2@gmail.com','$2y$12$9CaBa1UNZg2wVQmOQFlxiORrMQASxDepokoTF9ExvWCnzwzQflXZq',0,'2015-05-24 07:43:13','2015-05-24 07:43:13',0,0,'client',10000,NULL,NULL,NULL,'01b3ceb49d5499b8ce04b788c1f8ef6558f6b1c2a9bfee148c71234116d68b79'),(60,'pruebaCliente','surname','clienteAuctioan2@gmail.com','$2y$12$XqNFr5QnIfC4U1vwUYT35e3wiI.8EV.8Ug.U6XULPZk/UWqcUcQa6',0,'2015-05-24 07:43:40','2015-05-24 07:43:40',0,0,'client',10000,NULL,NULL,NULL,'c77e777507b48faf497b67b5cc72d1416ae76ae2cc4aaf5dde432c0ee772b554'),(61,'a','a','awewewe','$2y$12$cePjP802U75867NoM309FOu9cVojXVAuhNrN0Z9PbeyN2tUdFvxbq',0,'2015-05-24 07:45:26','2015-05-24 07:45:26',0,0,'client',10000,NULL,NULL,NULL,'d99a8131e4ecc5c831dec68eb4f692c7d46e3e2dabb26a809a96df43bff4872d'),(63,'a','a','aasasa','$2y$12$WN.WvkLZrgT2.pv2Iee1NeTJnd/PbqWYfQhJIxBrjr00mFuEWvywS',0,'2015-05-24 07:46:11','2015-05-24 07:46:11',0,0,'client',10000,NULL,NULL,NULL,'3b7a09f9ff626a4a636be48d9c5765b92e1b6d917ee3550ab6135e63f56d8192'),(64,'a','a','a1212','$2y$12$1pW85FhohxBaCixepKYSqeLoay7HwTiLWMOb5gn8/KucX2bm2YJTC',0,'2015-05-24 07:46:58','2015-05-24 07:46:58',0,0,'client',10000,NULL,NULL,NULL,'56a670f148b169f37a641154dfcfecbcbb9d9e1e1172a2c2c7eaf595d8e2cc70'),(65,'a','a','a12awew','$2y$12$da.Vby8zQywhS4yT7pTpvOKJeHnJdM8UB93u0iAsSYAV5emQHkRMK',0,'2015-05-24 07:48:21','2015-05-24 07:48:21',0,0,'client',10000,NULL,NULL,NULL,'b2a5f71ffdcab781b3686d617c3c626d84c5c9f89f40e0d637e4043c18cc4909'),(67,'a','a','a12awewAS','$2y$12$2/KN7qwZQAFEBzHwwocreey2C92JO3x1xLvlcqJGuFAh0W5W7gzTy',0,'2015-05-24 08:02:38','2015-05-24 08:02:38',0,0,'client',10000,NULL,NULL,NULL,'678e9c6721d4381dc035f037bfec8c36395699d9b4688c30416baee4c573b17b'),(68,'a','a','a12awewASA','$2y$12$hzc/fy7I97dBXYeb4BI/5.16qj9oHte6e/qrp3VYalFKz/a7KhQte',0,'2015-05-24 08:03:12','2015-05-24 08:03:12',0,0,'client',10000,NULL,NULL,NULL,'7e42c3a6f4391b549974d788a37af490a18f276a26a293d637c449c88bd4ffd4'),(69,'ERE','AS','AS','$2y$12$K6Sok7/qXsCl0xNEM6aujePSGC5pDD4MiPfBgDb8jcTnlr4xEeu5S',0,'2015-05-24 08:04:23','2015-05-24 08:04:23',0,0,'client',10000,NULL,NULL,NULL,'7bee70e55f181d4c76c5aea271f26098f21b225effd5a00bee7345ea193beb67'),(70,'ere','er','asasas','$2y$12$jtx8yI1OU8rN65ITFtWURu3ZLFb3qmlnYvVw7H09xjfTmcClADS0i',0,'2015-05-24 08:05:10','2015-05-24 08:05:10',0,0,'client',10000,NULL,NULL,NULL,'2f7fbc6def260d065c3a0864b5db5f291d5123c5debd76368cd74d1a997116bd'),(71,'prueba','prueba','prueba@gmail.com','$2y$12$6SxlcahEDHdagpeD4TunpuNJgpp61DXCKwemqsKzMhfzii/h3ubG6',0,'2015-05-24 13:30:50','2015-05-24 13:30:50',0,0,'client',10000,NULL,NULL,NULL,'cf024a66a2175dea3333f11863ef6259356378e42b695f4cde3aa6b690f98dc3'),(72,'gonzalo','moreno','gonzalo@gmail.com','$2y$12$u0LfFIBpU7o7AxxO6NIe4eJ5HtQzH8JMBgfp28x9YGsfsT1Lf9.xW',0,'2015-05-24 13:35:43','2015-05-24 13:35:43',0,0,'client',10000,NULL,NULL,NULL,'215e83e333cb0abb9da830ae4911e7ecd66092fa28112d2a4a0caa7c7607a560'),(73,'nombre','apellidos','naxo8628@gmail2.com','$2y$12$P5G1Sq7H9iuXC4djhurh6uoVO4BqsbeAn61BgXWaHrkaHB4i89Ira',4223,'2015-05-24 13:38:53','2015-05-24 13:38:53',0,0,'shipper',10000,'prueba2',0,'  ','c9808b69eedf335a42d31161bbd4632453d3ef5f6c5347b49ade0ebfa20f03da'),(74,'once56','apel','once56@gmail.com','$2y$12$80R2X.kyp6tGTxZDTYSvzODwcLqAI9cI4NWQo9iKHkYtK/sboi6Ci',0,'2015-06-01 23:57:34','2015-06-01 23:57:34',0,0,'client',10000,NULL,NULL,NULL,'e492025d3457bef23a5fb94f833eba48d27724e4d2bffa5a30188295ac4db7df'),(75,'doce40','apellidos','doce40@gmail.com','$2y$12$cxzCPnZ1SCyEGLG02A/Gj.AO.dD5KKr4zmJ8H6fDvrahnT8pwnUXO',0,'2015-06-02 00:41:41','2015-06-02 00:41:41',0,0,'client',10000,NULL,NULL,NULL,'843bde17848ce6a4e5f76d71f38246d88e885dc58c6921f2f7a655e8e4fa1502'),(76,'rep','apo','as@er.com','$2y$12$iggGFRwlYFn702VVp.j8Dus2hjJZ3Wp1OOn.VHB9bu9fBnvfQ9uKm',0,'2015-06-02 06:02:39','2015-06-02 06:02:39',0,0,'client',10000,NULL,NULL,NULL,'f9500910d50126d6b0429953e531fe18f73b79e8e383fdf81ea740be44dbcd1f'),(77,'shipper','tres08','ship@gmail.com','$2y$12$A95tHSzvFbr9g7lqDeAhAebrFX14nQoYbJ1yGIA4qssQcyxjB3d9i',8989,'2015-06-03 03:09:07','2015-06-03 03:09:07',0,0,'shipper',10000,'MRV',0,'  ','770c85d0f6f8a3cf4026ce1ce998f4998619efa5b8133f82585f724e3811d3e8'),(79,'a','a','a23','$2y$12$sQ8cGGM71CPA1nM9CFZdW.Oh9i64YlFg0TthQsG4OQEQWFzb.3s0C',12,'2015-06-03 03:11:51','2015-06-03 03:11:51',0,0,'shipper',10000,'mrv',0,'  ','4a7fd95df422b78dec842a643908e8292e8bffc6c19ec31e4ff33099e0edeb4c'),(80,'a','a','a2332','$2y$12$jjE10Ig9801bo5sdqG5PIucOloxkTAGMOZh3S6YERiiYtJ1xv613a',32,'2015-06-03 03:13:17','2015-06-03 03:13:17',0,0,'shipper',10000,'a',0,'  ','bf5e34f44ac77711928b55cfdb4ca851a4d5d07cd029308da5a9ca978b250ef1'),(81,'aasas','a','a2323','$2y$12$J9ew92VjRArM5iY/0/n6EuhF8RhXyRncjwxneNCc/nx.F/JCdVlCC',12,'2015-06-03 03:19:23','2015-06-03 03:19:23',0,0,'shipper',10000,'a',0,'  ','070d61493eb135a31ebd3767497f304bf38253c2d43f8d853905cb990afe9629'),(82,'a','a','qwerty','$2y$12$92m0oUbvW.E1Uj1MYUCmHOTbzEUdtDFaLQVF3CsYdVINqJhgfdzAm',12,'2015-06-03 03:29:11','2015-06-03 03:29:11',0,0,'shipper',10000,'a',0,'  ','ba33e74c490cda27c0fa9b2070db7b0fa312672b2f0f0f2b0c074c39d6ddae32'),(83,'gonzalo','m','gonzalo1@gmail.com','$2y$12$xFVsdjd9u7THuboc2wTese7J0LzIG3fJAUlfEMMRvz6WEEzlcvuUa',0,'2015-06-03 20:04:32','2015-06-03 20:04:32',0,0,'client',10000,NULL,NULL,NULL,'1d4b9c9a6965b8119abbad1430f7e0985d79a789391ac4c555fd5fd9c1a62792'),(84,'prueba','prueba','shipper20@gmail.com','$2y$12$u3i6UjobLHPOwPF4Lj/AH.u0BAvMYnW/NU0/UWZ0qHqQwgIPFURsi',909,'2015-06-03 20:05:26','2015-06-03 20:05:26',0,0,'shipper',10000,'mrv',0,'  ','e953444257f4f926b29b533f086231472ade3712e5c4cbd462c65e4659816195'),(85,'a','a','shipper21@gmail.com','$2y$12$RbL6IAdof62.qM3m.9gPLOmigAYk9qzUYkfZMpk9fT4CXjG0tW27C',1,'2015-06-03 20:10:06','2015-06-03 20:10:06',0,0,'shipper',10000,'a',0,'  ','fa841b774ccf237deabde6c0ad54371478498e352bdaa55cdc94d644d8b1a290'),(87,'a','a','a212323232323','$2y$12$vxqFACnuNTY08mNn894AVu8mR9dN8Y4UwwL6/p2s9Xxy2HaP0Xnl2',12,'2015-06-12 13:34:40','2015-06-12 13:34:40',0,0,'shipper',10000,'a',0,'  ','cee695e648e29ab378921d34603706a2717f4b52b7370fd82d4d14f333d7fc33');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Valuation`
--

DROP TABLE IF EXISTS `Valuation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Valuation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_from_id` int(11) DEFAULT NULL,
  `user_to_id` int(11) DEFAULT NULL,
  `auction_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A39F943A20C3C701` (`user_from_id`),
  KEY `IDX_A39F943AD2F7B13D` (`user_to_id`),
  KEY `IDX_A39F943A57B8F0DE` (`auction_id`),
  CONSTRAINT `FK_A39F943A20C3C701` FOREIGN KEY (`user_from_id`) REFERENCES `User` (`id`),
  CONSTRAINT `FK_A39F943A57B8F0DE` FOREIGN KEY (`auction_id`) REFERENCES `Auction` (`id`),
  CONSTRAINT `FK_A39F943AD2F7B13D` FOREIGN KEY (`user_to_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Valuation`
--

LOCK TABLES `Valuation` WRITE;
/*!40000 ALTER TABLE `Valuation` DISABLE KEYS */;
/*!40000 ALTER TABLE `Valuation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipper_certificate`
--

DROP TABLE IF EXISTS `shipper_certificate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipper_certificate` (
  `shipper_id` int(11) NOT NULL,
  `certificate_id` int(11) NOT NULL,
  PRIMARY KEY (`shipper_id`,`certificate_id`),
  KEY `IDX_EC41B49A38459F23` (`shipper_id`),
  KEY `IDX_EC41B49A99223FFD` (`certificate_id`),
  CONSTRAINT `FK_EC41B49A38459F23` FOREIGN KEY (`shipper_id`) REFERENCES `User` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_EC41B49A99223FFD` FOREIGN KEY (`certificate_id`) REFERENCES `Certificate` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipper_certificate`
--

LOCK TABLES `shipper_certificate` WRITE;
/*!40000 ALTER TABLE `shipper_certificate` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipper_certificate` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-25  2:15:55

<?php

namespace Blablapack\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ShipperType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('surname')
            ->add('email')
            ->add('password')
            ->add('phone')
            ->add('configuration','text',array('empty_data' => "10000"))
            ->add('company','text',array('empty_data' => '  '))
            ->add('transportType','text',array('empty_data' => "0"))
            ->add('transportDescription','text',array('empty_data' => '  '))
            ->add('certificates')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blablapack\FrontBundle\Entity\Shipper'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'blablapack_frontbundle_shipper';
    }
}

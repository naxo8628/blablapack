<?php

namespace Blablapack\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ValuationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type')
            ->add('comment')
            ->add('created')
            ->add('userFrom')
            ->add('userTo')
            ->add('auction')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blablapack\FrontBundle\Entity\Valuation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'blablapack_frontbundle_valuation';
    }
}

<?php

namespace Blablapack\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CertificateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description','text',array('empty_data' => '  '))
            ->add('icon','text',array('empty_data' => 'note-add2'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blablapack\FrontBundle\Entity\Certificate'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'certificate';
    }
}

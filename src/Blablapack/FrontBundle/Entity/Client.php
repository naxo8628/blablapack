<?php

namespace Blablapack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Blablapack\FrontBundle\Entity\ClientRepository")
 */
class Client extends User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Auction", mappedBy="client")
     **/
    private $auctions;

    /**
     * @var integer
     *
     * @ORM\Column(name="configuration", type="integer")
     */
    private $configuration;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->configuration = 10000;
        $this->auctions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * toString
     */
    public function __toString()
    {
        return $this->getName()." ".$this->getSurname();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return parent::getId();
    }

    /**
     * Set configuration
     *
     * @param integer $configuration
     * @return Client
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;

        return $this;
    }

    /**
     * Get configuration
     *
     * @return integer 
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * Add auctions
     *
     * @param \Blablapack\FrontBundle\Entity\Auction $auctions
     * @return Client
     */
    public function addAuction(\Blablapack\FrontBundle\Entity\Auction $auctions)
    {
        $this->auctions[] = $auctions;

        return $this;
    }

    /**
     * Remove auctions
     *
     * @param \Blablapack\FrontBundle\Entity\Auction $auctions
     */
    public function removeAuction(\Blablapack\FrontBundle\Entity\Auction $auctions)
    {
        $this->auctions->removeElement($auctions);
    }

    /**
     * Get auctions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAuctions()
    {
        return $this->auctions;
    }
}

<?php

namespace Blablapack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bid
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Blablapack\FrontBundle\Entity\BidRepository")
 */
class Bid
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal")
     */
    private $price;

    /**
     * @var integer
     *
     * 1-head; 0-superated; 1-accepted; 2-refused
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status=1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="Travel", inversedBy="bids")
     * @ORM\JoinColumn(name="travel_id", referencedColumnName="id")
     **/
    private $travel;

    /**
     * @ORM\ManyToOne(targetEntity="Auction", inversedBy="bids")
     * @ORM\JoinColumn(name="auction_id", referencedColumnName="id")
     **/
    private $auction;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Bid
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Bid
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set travel
     *
     * @param \Blablapack\FrontBundle\Entity\Travel $travel
     * @return Bid
     */
    public function setTravel(\Blablapack\FrontBundle\Entity\Travel $travel = null)
    {
        $this->travel = $travel;

        return $this;
    }

    /**
     * Get travel
     *
     * @return \Blablapack\FrontBundle\Entity\Travel 
     */
    public function getTravel()
    {
        return $this->travel;
    }

    /**
     * Set auction
     *
     * @param \Blablapack\FrontBundle\Entity\Auction $auction
     * @return Bid
     */
    public function setAuction(\Blablapack\FrontBundle\Entity\Auction $auction = null)
    {
        $this->auction = $auction;

        return $this;
    }

    /**
     * Get auction
     *
     * @return \Blablapack\FrontBundle\Entity\Auction 
     */
    public function getAuction()
    {
        return $this->auction;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Bid
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created = new \DateTime();
        $this->status = 0;
    }
}

<?php

namespace Blablapack\FrontBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TravelRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TravelRepository extends EntityRepository
{

    public function getByShipper($idShipper)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('Travel')
            ->from('FrontBundle:Travel', 'Travel')
            ->leftJoin('Travel.bids', 'Bid')
            ->Where('Bid.email = :email')
            ->setParameters(array('id' => $idShipper));

        if (!$qb->getQuery()->getArrayResult()) {
            return false;
        }

        $data = $qb->getQuery()->getArrayResult();
        return $data[0];
    }
}

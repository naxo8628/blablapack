<?php

namespace Blablapack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Travel
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Blablapack\FrontBundle\Entity\TravelRepository")
 */
class Travel implements \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="origin", type="string", length=255)
     */
    private $origin;

    /**
     * @var string
     *
     * @ORM\Column(name="destination", type="string", length=255)
     */
    private $destination;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status=0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="Shipper", inversedBy="travels")
     * @ORM\JoinColumn(name="shipper_id", referencedColumnName="id")
     **/
    private $shipper;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     **/
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Bid", mappedBy="travel")
     **/
    private $bids;

    /**
     * @ORM\OneToMany(targetEntity="Auction", mappedBy="travel")
     **/
    private $auctionsWins;

    /**
     * @ORM\OneToMany(targetEntity="Halt", mappedBy="travel")
     **/
    private $halts;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set origin
     *
     * @param string $origin
     * @return Travel
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return string 
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set destination
     *
     * @param string $destination
     * @return Travel
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return string 
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Travel
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Travel
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Travel
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bids = new \Doctrine\Common\Collections\ArrayCollection();
        $this->auctionsWins = new \Doctrine\Common\Collections\ArrayCollection();
        $this->halts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->created = new \DateTime();
    }

    /**
     * Set shipper
     *
     * @param \Blablapack\FrontBundle\Entity\Shipper $shipper
     * @return Travel
     */
    public function setShipper(\Blablapack\FrontBundle\Entity\Shipper $shipper = null)
    {
        $this->shipper = $shipper;

        return $this;
    }

    /**
     * Get shipper
     *
     * @return \Blablapack\FrontBundle\Entity\Shipper 
     */
    public function getShipper()
    {
        return $this->shipper;
    }

    /**
     * Set category
     *
     * @param \Blablapack\FrontBundle\Entity\Category $category
     * @return Travel
     */
    public function setCategory(\Blablapack\FrontBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Blablapack\FrontBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add bids
     *
     * @param \Blablapack\FrontBundle\Entity\Bid $bids
     * @return Travel
     */
    public function addBid(\Blablapack\FrontBundle\Entity\Bid $bids)
    {
        $this->bids[] = $bids;

        return $this;
    }

    /**
     * Remove bids
     *
     * @param \Blablapack\FrontBundle\Entity\Bid $bids
     */
    public function removeBid(\Blablapack\FrontBundle\Entity\Bid $bids)
    {
        $this->bids->removeElement($bids);
    }

    /**
     * Get bids
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBids()
    {
        return $this->bids;
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->category,
            $this->date,
            $this->origin,
            $this->destination,
            // see section on salt below
            // $this->salt,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Add auctionsWin
     *
     * @param \Blablapack\FrontBundle\Entity\Auction $auctionsWin
     *
     * @return Travel
     */
    public function addAuctionsWin(\Blablapack\FrontBundle\Entity\Auction $auctionsWin)
    {
        $this->auctionsWins[] = $auctionsWin;

        return $this;
    }

    /**
     * Remove auctionsWin
     *
     * @param \Blablapack\FrontBundle\Entity\Auction $auctionsWin
     */
    public function removeAuctionsWin(\Blablapack\FrontBundle\Entity\Auction $auctionsWin)
    {
        $this->auctionsWins->removeElement($auctionsWin);
    }

    /**
     * Get auctionsWins
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuctionsWins()
    {
        return $this->auctionsWins;
    }

    /**
     * Add halt
     *
     * @param \Blablapack\FrontBundle\Entity\Halt $halt
     *
     * @return Travel
     */
    public function addHalt(\Blablapack\FrontBundle\Entity\Halt $halt)
    {
        $this->halts[] = $halt;

        return $this;
    }

    /**
     * Remove halt
     *
     * @param \Blablapack\FrontBundle\Entity\Halt $halt
     */
    public function removeHalt(\Blablapack\FrontBundle\Entity\Halt $halt)
    {
        $this->halts->removeElement($halt);
    }

    /**
     * Get halts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHalts()
    {
        return $this->halts;
    }
}

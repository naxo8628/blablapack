<?php

namespace Blablapack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Auction
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Blablapack\FrontBundle\Entity\AuctionRepository")
 */
class Auction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="origin", type="string", length=255)
     */
    private $origin;

    /**
     * @var string
     *
     * @ORM\Column(name="destination", type="string", length=255)
     */
    private $destination;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal",nullable=true)
     */
    private $price = null;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expired", type="datetime")
     */
    private $expired;

    /**
     * @var integer
     *
     * @ORM\Column(name="periodicity", type="integer")
     */
    private $periodicity;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_bids", type="integer")
     */
    private $numBids=0;

    /**
     * @var integer
     *
     * 0-default 1-acepted 2-canceled
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status=0;

    /**
     * @ORM\OneToMany(targetEntity="Valuation", mappedBy="auction")
     **/
    private $valuations;

    /**
     * @ORM\OneToMany(targetEntity="Package", mappedBy="auction")
     **/
    private $packages;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="auctions")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     **/
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="Travel", inversedBy="auctionsWins")
     * @ORM\JoinColumn(name="travel_id", referencedColumnName="id", nullable=true)
     **/
    private $travel = null;
    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     **/
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Bid", mappedBy="auction")
     **/
    private $bids;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set origin
     *
     * @param string $origin
     * @return Auction
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return string 
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set destination
     *
     * @param string $destination
     * @return Auction
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return string 
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Auction
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Auction
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set expired
     *
     * @param \DateTime $expired
     * @return Auction
     */
    public function setExpired($expired)
    {
        $this->expired = $expired;

        return $this;
    }

    /**
     * Get expired
     *
     * @return \DateTime 
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * Set periodicity
     *
     * @param integer $periodicity
     * @return Auction
     */
    public function setPeriodicity($periodicity)
    {
        $this->periodicity = $periodicity;

        return $this;
    }

    /**
     * Get periodicity
     *
     * @return integer 
     */
    public function getPeriodicity()
    {
        return $this->periodicity;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Auction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->valuations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->packages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bids = new \Doctrine\Common\Collections\ArrayCollection();
        $this->created = new \DateTime();
        $this->periodicity = 0;
        $this->status = 0;
        $this->price = null;
    }

    /**
     * Add valuations
     *
     * @param \Blablapack\FrontBundle\Entity\Valuation $valuations
     * @return Auction
     */
    public function addValuation(\Blablapack\FrontBundle\Entity\Valuation $valuations)
    {
        $this->valuations[] = $valuations;

        return $this;
    }

    /**
     * Remove valuations
     *
     * @param \Blablapack\FrontBundle\Entity\Valuation $valuations
     */
    public function removeValuation(\Blablapack\FrontBundle\Entity\Valuation $valuations)
    {
        $this->valuations->removeElement($valuations);
    }

    /**
     * Get valuations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValuations()
    {
        return $this->valuations;
    }

    /**
     * Add packages
     *
     * @param \Blablapack\FrontBundle\Entity\Package $packages
     * @return Auction
     */
    public function addPackage(\Blablapack\FrontBundle\Entity\Package $packages)
    {
        $this->packages[] = $packages;

        return $this;
    }

    /**
     * Remove packages
     *
     * @param \Blablapack\FrontBundle\Entity\Package $packages
     */
    public function removePackage(\Blablapack\FrontBundle\Entity\Package $packages)
    {
        $this->packages->removeElement($packages);
    }

    /**
     * Get packages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPackages()
    {
        return $this->packages;
    }

    /**
     * Set auction
     *
     * @param \Blablapack\FrontBundle\Entity\Auction $auction
     * @return Auction
     */
    public function setAuction(\Blablapack\FrontBundle\Entity\Auction $auction = null)
    {
        $this->auction = $auction;

        return $this;
    }

    /**
     * Get auction
     *
     * @return \Blablapack\FrontBundle\Entity\Auction 
     */
    public function getAuction()
    {
        return $this->auction;
    }

    /**
     * Set category
     *
     * @param \Blablapack\FrontBundle\Entity\Category $category
     * @return Auction
     */
    public function setCategory(\Blablapack\FrontBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Blablapack\FrontBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add bids
     *
     * @param \Blablapack\FrontBundle\Entity\Bid $bids
     * @return Auction
     */
    public function addBid(\Blablapack\FrontBundle\Entity\Bid $bids)
    {
        $this->bids[] = $bids;
        $this->numBids++;

        return $this;
    }

    /**
     * Remove bids
     *
     * @param \Blablapack\FrontBundle\Entity\Bid $bids
     */
    public function removeBid(\Blablapack\FrontBundle\Entity\Bid $bids)
    {
        $this->bids->removeElement($bids);
        $this->numBids--;
    }

    /**
     * Get bids
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBids()
    {
        return $this->bids;
    }

    /**
     * Set client
     *
     * @param \Blablapack\FrontBundle\Entity\Client $client
     * @return Auction
     */
    public function setClient(\Blablapack\FrontBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Blablapack\FrontBundle\Entity\Client 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set travel
     *
     * @param \Blablapack\FrontBundle\Entity\Travvel $travel
     *
     * @return Auction
     */
    public function setTravel(\Blablapack\FrontBundle\Entity\Travel $travel = null)
    {
        $this->travel = $travel;

        return $this;
    }

    /**
     * Get travel
     *
     * @return \Blablapack\FrontBundle\Entity\Travel
     */
    public function getTravel()
    {
        return $this->travel;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Auction
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set numBids
     *
     * @param integer $numBids
     *
     * @return Auction
     */
    public function setNumBids($numBids)
    {
        $this->numBids = $numBids;

        return $this;
    }

    /**
     * Get numBids
     *
     * @return integer
     */
    public function getNumBids()
    {
        return $this->numBids;
    }
}

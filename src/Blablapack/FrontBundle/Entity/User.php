<?php

namespace Blablapack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Entity(repositoryClass="Blablapack\FrontBundle\Entity\UserRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="user_type", type="string")
 * @ORM\DiscriminatorMap({"user" = "User", "client" = "Client", "admin" = "Admin", "shipper"= "Shipper"})
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255, unique=true)
     */
    private $hash;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var integer
     *
     * @ORM\Column(name="phone", type="integer")
     */
    private $phone=0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_conection", type="datetime")
     */
    private $lastConection;

    /**
     * @var integer
     *
     * @ORM\Column(name="validate_email", type="integer")
     */
    private $validateEmail;

    /**
     * @var integer
     *
     * @ORM\Column(name="validate_phone", type="integer")
     */
    private $validatePhone;

    /**
     * @ORM\OneToMany(targetEntity="Valuation", mappedBy="userFrom")
     **/
    private $valuationsSend;

    /**
     * @ORM\OneToMany(targetEntity="Valuation", mappedBy="userTo")
     **/
    private $valuationsRecive;

    /**
     * Constructor
     */
    public function __construct()
    {

        $this->valuationsSend = new \Doctrine\Common\Collections\ArrayCollection();
        $this->valuationsRecive = new \Doctrine\Common\Collections\ArrayCollection();
        $this->created = new \DateTime();
        $this->lastConection = new \DateTime();
        $this->validateEmail = 0;
        $this->validatePhone = 0;
    }

    /**
     * get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        $this->hash = hash("sha256",$this->name.strtotime("now"));

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Check password
     *
     * @param string $password
     * @return User
     */
    public function checkPassword($password)
    {
        if (!is_null($password)) {
            return $this->password == password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
        }

        return false;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        if (!is_null($password)) {
            return $this->password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
        }

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set phone
     *
     * @param integer $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return integer 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return User
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set lastConection
     *
     * @param \DateTime $lastConection
     * @return User
     */
    public function setLastConection($lastConection)
    {
        $this->lastConection = $lastConection;

        return $this;
    }

    /**
     * Get lastConection
     *
     * @return \DateTime 
     */
    public function getLastConection()
    {
        return $this->lastConection;
    }

    /**
     * Set validateEmail
     *
     * @param integer $validateEmail
     * @return User
     */
    public function setValidateEmail($validateEmail)
    {
        $this->validateEmail = $validateEmail;

        return $this;
    }

    /**
     * Get validateEmail
     *
     * @return integer 
     */
    public function getValidateEmail()
    {
        return $this->validateEmail;
    }

    /**
     * Set validatePhone
     *
     * @param integer $validatePhone
     * @return User
     */
    public function setValidatePhone($validatePhone)
    {
        $this->validatePhone = $validatePhone;

        return $this;
    }

    /**
     * Get validatePhone
     *
     * @return integer 
     */
    public function getValidatePhone()
    {
        return $this->validatePhone;
    }

    /**
     * Add valuationsSend
     *
     * @param \Blablapack\FrontBundle\Entity\Valuation $valuationsSend
     * @return User
     */
    public function addValuationsSend(\Blablapack\FrontBundle\Entity\Valuation $valuationsSend)
    {
        $this->valuationsSend[] = $valuationsSend;

        return $this;
    }

    /**
     * Remove valuationsSend
     *
     * @param \Blablapack\FrontBundle\Entity\Valuation $valuationsSend
     */
    public function removeValuationsSend(\Blablapack\FrontBundle\Entity\Valuation $valuationsSend)
    {
        $this->valuationsSend->removeElement($valuationsSend);
    }

    /**
     * Get valuationsSend
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValuationsSend()
    {
        return $this->valuationsSend;
    }

    /**
     * Add valuationsRecive
     *
     * @param \Blablapack\FrontBundle\Entity\Valuation $valuationsRecive
     * @return User
     */
    public function addValuationsRecive(\Blablapack\FrontBundle\Entity\Valuation $valuationsRecive)
    {
        $this->valuationsRecive[] = $valuationsRecive;

        return $this;
    }

    /**
     * Remove valuationsRecive
     *
     * @param \Blablapack\FrontBundle\Entity\Valuation $valuationsRecive
     */
    public function removeValuationsRecive(\Blablapack\FrontBundle\Entity\Valuation $valuationsRecive)
    {
        $this->valuationsRecive->removeElement($valuationsRecive);
    }

    /**
     * Get valuationsRecive
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getValuationsRecive()
    {
        return $this->valuationsRecive;
    }

    /**
     * Set hash
     *
     * @param string $hash
     *
     * @return User
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        //switch($this->){

        //    case 0:
        return array('ROLE_USER');

        //    case 1: return array('ROLE_HOTEL');

         //   case 2: return array('ROLE_HOTEL_ADMIN');
        //}

    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
        return null;
    }
}

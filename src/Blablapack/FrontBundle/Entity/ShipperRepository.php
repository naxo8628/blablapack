<?php

namespace Blablapack\FrontBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ShipperRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ShipperRepository extends EntityRepository
{
}

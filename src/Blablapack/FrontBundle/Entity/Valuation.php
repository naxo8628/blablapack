<?php

namespace Blablapack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Valuation
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Blablapack\FrontBundle\Entity\ValuationRepository")
 */
class Valuation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="valuationsSend")
     * @ORM\JoinColumn(name="user_from_id", referencedColumnName="id")
     **/
    private $userFrom;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="valuationsRecive")
     * @ORM\JoinColumn(name="user_to_id", referencedColumnName="id")
     **/
    private $userTo;

    /**
     * @ORM\ManyToOne(targetEntity="Auction", inversedBy="valuations")
     * @ORM\JoinColumn(name="auction_id", referencedColumnName="id")
     **/
    private $auction;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Valuation
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Valuation
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Valuation
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set userFrom
     *
     * @param \Blablapack\FrontBundle\Entity\User $userFrom
     * @return Valuation
     */
    public function setUserFrom(\Blablapack\FrontBundle\Entity\User $userFrom = null)
    {
        $this->userFrom = $userFrom;

        return $this;
    }

    /**
     * Get userFrom
     *
     * @return \Blablapack\FrontBundle\Entity\User 
     */
    public function getUserFrom()
    {
        return $this->userFrom;
    }

    /**
     * Set userTo
     *
     * @param \Blablapack\FrontBundle\Entity\User $userTo
     * @return Valuation
     */
    public function setUserTo(\Blablapack\FrontBundle\Entity\User $userTo = null)
    {
        $this->userTo = $userTo;

        return $this;
    }

    /**
     * Get userTo
     *
     * @return \Blablapack\FrontBundle\Entity\User 
     */
    public function getUserTo()
    {
        return $this->userTo;
    }

    /**
     * Set auction
     *
     * @param \Blablapack\FrontBundle\Entity\Auction $auction
     * @return Valuation
     */
    public function setAuction(\Blablapack\FrontBundle\Entity\Auction $auction = null)
    {
        $this->auction = $auction;

        return $this;
    }

    /**
     * Get auction
     *
     * @return \Blablapack\FrontBundle\Entity\Auction 
     */
    public function getAuction()
    {
        return $this->auction;
    }
}

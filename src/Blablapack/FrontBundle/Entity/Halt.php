<?php

namespace Blablapack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Halt
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class Halt
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal")
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal")
     */
    private $latitude;

    /**
     * @ORM\ManyToOne(targetEntity="Travel", inversedBy="halts")
     * @ORM\JoinColumn(name="travel_id", referencedColumnName="id")
     **/
    private $travel;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Halt
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Halt
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Halt
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set travel
     *
     * @param \Blablapack\FrontBundle\Entity\Travel $travel
     *
     * @return Halt
     */
    public function setTravel(\Blablapack\FrontBundle\Entity\Travel $travel = null)
    {
        $this->travel = $travel;

        return $this;
    }

    /**
     * Get travel
     *
     * @return \Blablapack\FrontBundle\Entity\Travel
     */
    public function getTravel()
    {
        return $this->travel;
    }
}

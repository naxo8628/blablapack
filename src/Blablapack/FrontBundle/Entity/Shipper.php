<?php

namespace Blablapack\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shipper
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Blablapack\FrontBundle\Entity\ShipperRepository")
 */
class Shipper extends User
{


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="configuration", type="integer")
     */
    private $configuration;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255)
     */
    private $company;

    /**
     * @var integer
     *
     * @ORM\Column(name="transport_type", type="integer")
     */
    private $transportType;

    /**
     * @var string
     *
     * @ORM\Column(name="transport_description", type="string", length=255)
     */
    private $transportDescription;

    /**
     * @ORM\OneToMany(targetEntity="Travel", mappedBy="shipper")
     **/
    private $travels;

    /**
     * @ORM\ManyToMany(targetEntity="Certificate")
     * 
     **/
    private $certificates;


    /**
     * Set company
     *
     * @param string $company
     * @return Shipper
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * get Id
     *
     * @param string $company
     * @return Shipper
     */
    public function getId()
    {
        return parent::getId();
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set transportType
     *
     * @param integer $transportType
     * @return Shipper
     */
    public function setTransportType($transportType)
    {
        $this->transportType = $transportType;

        return $this;
    }

    /**
     * Get transportType
     *
     * @return integer 
     */
    public function getTransportType()
    {
        return $this->transportType;
    }

    /**
     * Set transportDescription
     *
     * @param string $transportDescription
     * @return Shipper
     */
    public function setTransportDescription($transportDescription)
    {
        $this->transportDescription = $transportDescription;

        return $this;
    }

    /**
     * Get transportDescription
     *
     * @return string 
     */
    public function getTransportDescription()
    {
        return $this->transportDescription;
    }

    /**
     * Set configuration
     *
     * @param integer $configuration
     * @return Client
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;

        return $this;
    }

    /**
     * Get configuration
     *
     * @return integer
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->travels = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add travels
     *
     * @param \Blablapack\FrontBundle\Entity\Travel $travels
     * @return Shipper
     */
    public function addTravel(\Blablapack\FrontBundle\Entity\Travel $travels)
    {
        $this->travels[] = $travels;

        return $this;
    }

    /**
     * Remove travels
     *
     * @param \Blablapack\FrontBundle\Entity\Travel $travels
     */
    public function removeTravel(\Blablapack\FrontBundle\Entity\Travel $travels)
    {
        $this->travels->removeElement($travels);
    }

    /**
     * Get travels
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTravels()
    {
        return $this->travels;
    }

    /**
     * Add certificate
     *
     * @param \Blablapack\FrontBundle\Entity\Certificate $certificate
     *
     * @return Shipper
     */
    public function addCertificate(\Blablapack\FrontBundle\Entity\Certificate $certificate)
    {
        $this->certificates[] = $certificate;

        return $this;
    }

    /**
     * Remove certificate
     *
     * @param \Blablapack\FrontBundle\Entity\Certificate $certificate
     */
    public function removeCertificate(\Blablapack\FrontBundle\Entity\Certificate $certificate)
    {
        $this->certificates->removeElement($certificate);
    }

    /**
     * Get certificates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCertificates()
    {
        return $this->certificates;
    }
}

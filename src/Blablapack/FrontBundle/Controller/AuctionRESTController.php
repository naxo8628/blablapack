<?php

namespace Blablapack\FrontBundle\Controller;

use Blablapack\FrontBundle\Entity\Auction;
use Blablapack\FrontBundle\Entity\Bid;
use Blablapack\FrontBundle\Entity\Client;
use Blablapack\FrontBundle\Entity\User;
use Blablapack\FrontBundle\Form\AuctionType;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * Auction controller.
 * @RouteResource("Auction")
 */
class AuctionRESTController extends VoryxController
{
    /**
     * Get a Auction entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(User $client,Auction $entity)
    {
        return $entity;
    }
    /**
     * Get all Auction entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(User $client,ParamFetcherInterface $paramFetcher)
    {
        try {
            $offset = $paramFetcher->get('offset');
            $limit = $paramFetcher->get('limit');
            $order_by = $paramFetcher->get('order_by');
            $filters = !is_null($paramFetcher->get('filters')) ? $paramFetcher->get('filters') : array();

            $em = $this->getDoctrine()->getManager();
            $clientReal = $em->getRepository('FrontBundle:Client')->find($client->getId());
            if($clientReal){
                $entities = $em->getRepository('FrontBundle:Auction')->getByClient($client->getId());
            }else{
                $entities = $em->getRepository('FrontBundle:Auction')->getByAll();
            }
            if ($entities) {
                return $entities;
            }

            return new Response('Not Found', Codes::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get all Auction entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetSearchAction(Request $request)
    {
        try {
            $data = $request->query->all();
            $date = new \DateTime($data['date']);

            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('FrontBundle:Auction')->getByLocation($date,$data['origin'],$data['destination']);
            if ($entities) {
                return $entities;
            }

            return new Response('Not Found', Codes::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Create a Auction entity.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request,Client $client)
    {
        $data = $request->request->all();
        $entity = new Auction();
        $entity->setClient($client);
        $dateExpired = new \DateTime();
        $dateExpired->modify("+ ".$data['duration']." days");
        $entity->setExpired($dateExpired);
        $form = $this->createForm(new AuctionType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $client->addAuction($entity);
            $em->persist($entity);
            $em->flush();

            return $entity;
        }

        return new Response($form->getErrors(true,false), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
    /**
     * Update a Auction entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request,Client $client, Auction $entity)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(new AuctionType(), $entity, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->flush();

                return $entity;
            }

            return new Response($form->getErrors(true,false), Codes::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Partial Update to a Auction entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
*/
    public function patchAction(Request $request,Client $client, Auction $entity)
    {
        return $this->putAction($request,$client, $entity);
    }
    /**
     * Delete a Auction entity.
     *
     * @View(statusCode=204)
     *
     * @param Request $request
     * @param $entity
     * @internal param $id
     *
     * @return Response
     */
    public function deleteAction(Request $request,Client $client, Auction $entity)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            return null;
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get a Bid of a auction entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getBidAction(Client $client,Auction $entity,Bid $bid)
    {
        return $bid;
    }
}

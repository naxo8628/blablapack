<?php

namespace Blablapack\FrontBundle\Controller;

use Blablapack\FrontBundle\Entity\Client;
use Blablapack\FrontBundle\Entity\Auction;
use Blablapack\FrontBundle\Entity\Package;
use Blablapack\FrontBundle\Form\ClientType;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * Client controller.
 * @RouteResource("Client")
 */
class ClientRESTController extends VoryxController
{
    /**
     * Get a Client entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(Client $entity)
    {
        return $entity;
    }
    /**
     * Get all Client entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        try {
            $offset = $paramFetcher->get('offset');
            $limit = $paramFetcher->get('limit');
            $order_by = $paramFetcher->get('order_by');
            $filters = !is_null($paramFetcher->get('filters')) ? $paramFetcher->get('filters') : array();

            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('FrontBundle:Client')->findBy($filters, $order_by, $limit, $offset);
            if ($entities) {
                return $entities;
            }

            return new Response('Not Found', Codes::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Create a Client entity.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request)
    {
        $entity = new Client();
        $form = $this->createForm(new ClientType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $entity;
        }

        return new Response($form->getErrors(true,false), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
    /**
     * Create a Client entity with first auction
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postPlusAction(Request $request)
    {
        $data = $request->getContent();
        $data = json_decode($data,true);
        $em = $this->getDoctrine()->getManager();
        if(!is_numeric($data['days']) || $data['days']< 1 || $data['days']> 60){
            return "La subasta tiene que durar entre 1 y 60 días";
        }
        if(isset($data['hash']) && $data['hash'] != "" && $data['hash'] != null){

            $client = $em->getRepository('FrontBundle:Client')->findBy(array('hash' => $data['hash']));
            $client = $client[0];
        }else{
            $client = new Client();
            $client->setName($data['name']);
            $client->setSurname($data['surname']);
            $client->setEmail($data['email']);
            $client->setPassword($data['password']);
            $em->persist($client);

        }

        $auction = new Auction();
        $auction->setOrigin($data['origin']);
        $auction->setDestination($data['destination']);
        $dateExpired = new \DateTime();
        $dateExpired->modify("+ ".$data['days']." days");
        $dateExpired->modify("+ ".$data['hours']." hours");
        $auction->setExpired($dateExpired);
        $auction->setDuration($data['days']*24 + $data['hours']);
        $auction->setClient($client);
        $client->addAuction($auction);

        //$packs = json_decode($data["packs"]);
        //return $packs;
        foreach($data['packs'] as $pack){
            $newPack = new Package();
            $newPack->setAuction($auction);
            if(isset($pack['name'])){
                $newPack->setName($pack['name']);
            }
            $newPack->setHeight($pack['height']);
            $newPack->setLength($pack['length']);
            $newPack->setWeight($pack['weight']);
            $newPack->setWidth($pack['width']);
            $auction->addPackage($newPack);
            $em->persist($newPack);
        }

        $em->persist($auction);

        $em->flush();

        if(isset($data['response']) && $data['response'] == "auction"){

            return array('id' => $auction->getId());
        }

        return array('id' => $client->getId(),
                     'name' => $client->getName(),
                     'surname' => $client->getSurname(),
                     'email' => $client->getEmail(),
                     'phone' => $client->getPhone(),
                     'hash' => $client->getHash());

    }
    /**
     * Update a Client entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, Client $entity)
    {
        try {

            $em = $this->getDoctrine()->getManager();
            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(new ClientType(), $entity, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->flush();

                return $entity;
            }

            dump((string) $form->getErrors(true));
            return new Response((string) $form->getErrors(true, false), Codes::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Partial Update to a Client entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
*/
    public function patchAction(Request $request, Client $entity)
    {
        return $this->putAction($request, $entity);
    }
    /**
     * Delete a Client entity.
     *
     * @View(statusCode=204)
     *
     * @param Request $request
     * @param $entity
     * @internal param $id
     *
     * @return Response
     */
    public function deleteAction(Request $request, Client $entity)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            return null;
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get a Auction of a client entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAuctionAction(Client $client,Auction $entity)
    {
        return $entity;
    }
}

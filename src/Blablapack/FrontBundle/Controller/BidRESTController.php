<?php

namespace Blablapack\FrontBundle\Controller;

use Blablapack\FrontBundle\Entity\Bid;
use Blablapack\FrontBundle\Entity\Shipper;
use Blablapack\FrontBundle\Entity\Travel;
use Blablapack\FrontBundle\Entity\Auction;
use Blablapack\FrontBundle\Form\BidType;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * Bid controller.
 * @RouteResource("Bid")
 */
class BidRESTController extends VoryxController
{
    /**
     * Get a Bid entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(Travel $travel,Auction $auction,Bid $entity)
    {
        return $entity;
    }
    /**
     * Get all Bid entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(Travel $travel,Auction $auction,ParamFetcherInterface $paramFetcher)
    {
        try {
            $offset = $paramFetcher->get('offset');
            $limit = $paramFetcher->get('limit');
            $order_by = $paramFetcher->get('order_by');
            $filters = !is_null($paramFetcher->get('filters')) ? $paramFetcher->get('filters') : array();

            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('FrontBundle:Bid')->findBy($filters, $order_by, $limit, $offset);
            if ($entities) {
                return $entities;
            }

            return new Response('Not Found', Codes::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Create a Bid entity.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request,Shipper $shipper,Travel $travel,Auction $auction)
    {
        $data = $request->request->all();

        if($auction->getPrice() == null || ($auction->getPrice() - 0.5) >= $data['price']){
            $entity = new Bid();
            $entity->setPrice($data['price']);
            $entity->setTravel($travel);
            $entity->setAuction($auction);

            $travel->addBid($entity);

            $auction->addBid($entity);
            $auction->setTravel($travel);
            $auction->setPrice($data['price']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $entity;
        }

        $price = ($auction->getPrice() - "0.5");

        return "El precio tiene que ser menor a: ".$price."€";

    }

}

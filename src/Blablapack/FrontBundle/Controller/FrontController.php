<?php

namespace Blablapack\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FrontController extends Controller
{
    public function indexAction()
    {
        return $this->render('FrontBundle:Front:index.html.twig');
    }

    public function resultsAction()
    {
        return $this->render('FrontBundle:Front:results.html.twig');
    }
}

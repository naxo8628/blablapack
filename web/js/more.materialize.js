
//floatings btns with ampliable content
$(".btn-floating.drop i").click(function(){

    var h = $(this).parent().attr("data-h");
    var w = $(this).parent().attr("data-w");

    if( $(this).parent().hasClass("open")){
        $(this).parent().removeClass("open");
        $(this).parent().remove("height");
        $(this).parent().removeAttr("style");
        $(this).parent().removeClass("red");
        $(this).parent().addClass("blue");
    }else{
        $(this).parent().addClass("open");
        $(this).parent().removeClass("blue");
        $(this).parent().addClass("red");
        $(this).parent().height(h+"px");
        $(this).parent().width(w+"px");
    }
});

// resize cards
function sizeCard(card,newClassDimension){

    if(card.hasClass("s1")) card.removeClass("s1");
    if(card.hasClass("s2")) card.removeClass("s2");
    if(card.hasClass("s3")) card.removeClass("s3");
    if(card.hasClass("s4")) card.removeClass("s4");
    if(card.hasClass("s5")) card.removeClass("s5");
    if(card.hasClass("s6")) card.removeClass("s6");
    if(card.hasClass("s7")) card.removeClass("s7");
    if(card.hasClass("s8")) card.removeClass("s8");
    if(card.hasClass("s9")) card.removeClass("s9");
    if(card.hasClass("s10")) card.removeClass("s10");
    if(card.hasClass("s11")) card.removeClass("s11");
    if(card.hasClass("s12")) card.removeClass("s12");

    if(card.hasClass("m1")) card.removeClass("m1");
    if(card.hasClass("m2")) card.removeClass("m2");
    if(card.hasClass("m3")) card.removeClass("m3");
    if(card.hasClass("m4")) card.removeClass("m4");
    if(card.hasClass("m5")) card.removeClass("m5");
    if(card.hasClass("m6")) card.removeClass("m6");
    if(card.hasClass("m7")) card.removeClass("m7");
    if(card.hasClass("m8")) card.removeClass("m8");
    if(card.hasClass("m9")) card.removeClass("m9");
    if(card.hasClass("m10")) card.removeClass("m10");
    if(card.hasClass("m11")) card.removeClass("m11");
    if(card.hasClass("m12")) card.removeClass("m12");

    if(card.hasClass("l1")) card.removeClass("l1");
    if(card.hasClass("l2")) card.removeClass("l2");
    if(card.hasClass("l3")) card.removeClass("l3");
    if(card.hasClass("l4")) card.removeClass("l4");
    if(card.hasClass("l5")) card.removeClass("l5");
    if(card.hasClass("l6")) card.removeClass("l6");
    if(card.hasClass("l7")) card.removeClass("l7");
    if(card.hasClass("l8")) card.removeClass("l8");
    if(card.hasClass("l9")) card.removeClass("l9");
    if(card.hasClass("l10")) card.removeClass("l10");
    if(card.hasClass("l11")) card.removeClass("l11");
    if(card.hasClass("l12")) card.removeClass("l12");

    card.addClass(newClassDimension);


}
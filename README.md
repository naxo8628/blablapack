Polymer & Symfony Project v0.8
========================

Creación de una aplicación web para transporte de paquetes personales. El sistema consiste en el transporte de paquetes que publican usuarios que enviarlo a un cierto destino por parte de los transportistas que pujen por ese paquete, el que pague menos por llevarlo, ganará la subasta. El usuario podrá elegir crear una subasta con el precio que elija de salida y por la otra parte el transportista a la hora de entrar en su panel, podrá publicar sus viajes y tendrá una lista de paquetes subastados en los que podrá pujar en función de su viaje.

Gestión de Usuarios

Diferenciamos entre 3 tipos de usuario:


  * transportista: tendrá que registrarse antes de poder usar la aplicacion, podra crear sus viajes y pujar en las subastas de los usuarios. Podrá ver un listado de todos sus viajes y tendrá que actualizar el estado del viaje.

  * usuario: podrá hacer una subasta de un paquete o conjunto de paquetes con un precio inicial. Una vez registrado, podrá acceder a su panel con sus subastas y las pujas realizadas por los transportistas.

  * administrador: permisos absolutos, pueden ver todos los viajes con los diferentes estados y las últimas pujas, además del listado de usuarios y de transportistas.


Entidades


  * Transportista: usuario,contraseña,email,telefono,certificados,dni,compañía, descripción de automóvil, viajes publicados, fotos, valoraciones

  * Viaje: tipo de venta, hora de salida, flexibilidad en la hora de salida, origen, destino, espacio disponible, peso Max, precio metro cuadrado

  * Usuario: usuario,contraseña,email,teléfono, subastas publicadas

  * Subasta: duración, paquetes, origen, destino, precio Salida, fecha, flexibilidad, categoría

  * Paquete: alto,ancho,largo, peso, subasta asignada, fotos

  * Categoría: tipo categoría, nombre, descripción

  * Puja: subasta, transportista, hora, precio pujado

  * Valoración: tipo valoracion, comentario, usuario/transportista (tanto de usuarios como de transportistas se intercambian puntuaciones al terminar el viaje)


Vistas


  * Front (principal): es la pantalla de inicio y donde ira el proceso de subasta (fecha salida,flexibilidad en la salida, origen,destino), la información acerca de la aplicación(ventajas, ahorro..), "quienes somos", registro de usuario/ transportista, contacto,redes sociales...

  * Registro subasta: si se opta por la opción de subastar habrá un pequeño formulario para añadir sus paquetes  (peso y dimensiones), duración de la subasta  y precio de la subasta.

  * Fin subasta: un pequeño formulario que preguntará su nombre, mail,precio inicial subasta, y teléfono opcional. Se le enviará a un correo con la subasta generada con opción a cancelar y las actualizaciones de pujas que vayan apareciendo además de la puja ganadora con el contacto del conductor.

  * Panel usuario: el usuario una vez hecho una subasta queda registrado en el sistema, y todas las acciones que haga con ese email se enlazaran al mismo usuario. Podrá acceder a un menú con una lista de las subastas realizadas con su estado y pujas asociadas, ver los viajes en los que ha mandado paquetes y editar los datos básicos del usuario.

  * Panel transportista: se tendrá que registrar y una vez completado el registro podrá acceder al panel para publicar viajes. Tendrá su perfil para editar los datos de contacto, listado de viaje publicados, listado de subastas ganadas y un listado de valoraciones de los usuarios tras el viaje.

  * Panel superAdmin: este panel será solo para vosotros, desde este panel podreis ver todo lo que pasa en la aplicación, si elegis validar a los transportistas primero, será aquí donde vereis los candidatos y los aceptareis para que entren. desde aqui podreis modificar cualquier subasta o viaje o paquete que haya creado cualquier usuario/transportista, es necesario cuando tengais "atención al cliente" y puedan ser ellos los que lleven el proceso de mantenimiento.



Instalación inicial

El proyecto está desarrollado con Symfony2 en el backend para todas las peticiones que tenga que intervenir la logica de negocio, toda la documentacion del Framework MVC lo tienes en la pagina oficial (http://symfony.com/doc/current/index.html) . La Base de datos es Mysql diseñada con doctrine2, para que se genere la base de datos en local tendrás que usar los comandos de doctrine2 (http://doctrine-orm.readthedocs.org/en/latest/) y para su modificación.

En cuanto a frontend, utilizo Polymer de google (que es una libreria para crear webcomponents), muy similar al angularJS, donde puedo crear y reutilizar muchos de ellos, la documentacion la tienes aqui ( https://www.polymer-project.org/0.5/docs/polymer/polymer.html), todas las dependencias con los elementos creados por google se pueden actuzalizar ejecutando bower (http://bower.io/docs/api/) y modificando el correspondiente archivo bower.json. La carpeta de bower_components lo mejor es vaciarla y volver a generar todas las dependencias.

El Routing de todas las URL las hago mediante app-router ( es un web component de un tercero), que hace las peticiones por detrás, y una vez cargado el contenido lo muestra o realiza otra acción, evitando el refresco de la pagina y un mayor dinamismo.

Para la configuracion en local es necesario que levantes el servidor de symfony2 en local ( app/console server:start) y que configures los sitios de apache para que apunten a app.php de la carpeta web. Tendrás que sustituir blalbapack.es por tu localhost o en la IP donde te montes el servidor.
virtual host blablapack.png

Backend

Toda la lógica de negocio se encuentra en la carpeta src/Blablapack. Al no tener que generar vistas por cada una de las peticiones, sino que se van modificando los distintos web components según las peticiones, los controladores son de tipo REST (API), y todas las respuestas tendrán una respuesta en json.

Todas estas funciones se encuentran en los controladores y suelen usar los repositorios de cada una de las entidades para acceder a la base de datos. Ahora mismo están hechas muchas de las funciones GET,POST, PUT, DELETE para entidades individuales y grupales. Cada una de ellas muestra, crea, edita o elimina una o varias entidades.

La API tiene su configuracion en routingAPI.yml que apunta a cada controlador (cada vez que se cree una funcion en alguno de los controladores, se crea un ruta URL que ejecuta esa misma  funcion), ver mejor la documentacion de http://symfony.com/doc/current/bundles/FOSRestBundle/index.html.

En Cuanto a la carpeta con las entidades, todas tienen su objeto que mapea cada tabla, y los repositorios con funciones específicas para cada uno de ellos. Si cambias algo en el ORM de la entidad y luego ejecutas por consola doctrine update, se verá reflejado en Mysql el cambio.

Frontend

Es bastante necesario saber como funciona polymer, como funciona en bind de variables y como se comunican 2 webcompnents. El proyecto fronted está encabezado en un documento twig (index.html.twig) que carga las dependencias con polymer, con webcomponents y demas librerias de terceros, y aqui se definen todas las rutas posibles que tendrá la aplicacion web (NO LA API) y hace uso de la plantilla layout-base.html, que se encarga de dibujar el marco, menus, barra de estado, alertas flotantes, dialogo de login/registro, media queries que determinan en que dispositivo estamos, la animacion que tendran los difrentes contenidos y las peticiones ajax para conseguir el Hash del usuario.


Elementos creado y donde y como se usan y comunican:

  * element-search: es la página principal de inicio, donde se define el formulario de nueva subasta, añadir paquetes, datos de la subasta, servicios y finalizacion. Ademas de mostrar todos las secciones definidas en las variables features y features2. Y finalmente la seccion de  contacto. Este elemento usa el elemento my-pack.

  * element-success: elemento creado para mostrar un mensaje de Success y redirigir a otro elemento.

  * my-auction: es la vision general de una subasta, donde te muestra la informacion, las pujas ganadas y paquetes de esa subasta.

  * my-auction-bid: elemento que muestra una puja en concreto con su informacion ( se usa en my-travel)

  * my-auctions: elemento muestra una lista con todas las subastas para el usuario registrado y la opcion de crear una nueva.
  
  * my-bid: parecida a my-auction-bid pero esta tiene menos informacion ya que aun no es una puja ganadora

  * my-categories: vista para la edicion y creacion de nuevas categorias de envios

  * my-config: vista para la configuracion del perfil

  * my-pack: pequeño formulario pora la creacion de un paquete (medidas peso y nombre) que se usa en element-search y en my-auctions.

  * my-petition: antigua implementacion cuando los usuarios podian pedir viajes urgentes a unos transportistas en concreto.

  * my-profile: muestra estadisticas (ahora estaticas) de los viajes totales, falta de datos, o las notificaciones generadas.

  * my-profile-admin: idem anterio pero para admin

  * my-profile-client: idem pero para cliente.

  * my-request: antigua implementacion para crear una subasta

  * my-search: vista de busqueda de paquetes para el transportista

  * my-sections: elemento para la abstraccion de secciones del element-search (aun por hacer)

  * my-shippers: lista de todos transportistas que hay en el sistema y viajes publicados.

  * my-travel: vista detallada del viaje publicado por un transportista. usa my-auction-bid, my-bid y my-petition.

  * my-travels: lista de todos los viajes y posibilidad de crear uno nuevo (usa el elemento my-travel)


It configured with the following bundles:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * [**AsseticBundle**][12] - Adds support for Assetic, an asset processing
    library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev/test env) - Adds code generation
    capabilities
